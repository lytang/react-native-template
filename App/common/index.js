import _constant from './constant';
import configureStore from './storeConfig';

export const constant = _constant;
export const configureStores = configureStore;