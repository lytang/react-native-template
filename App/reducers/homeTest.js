import {TestReducer } from '../common/constant'
const initialState ={
  availableCollection:[]
}

const homeReducer=(state = initialState, action)=>{
    // alert('collection Action'+JSON.stringify(action))
  switch(action.type){
    case TestReducer:
      return{
        ...state,
        availableCollection:[]
    }
    default:
    return state
  }
}
export default homeReducer;