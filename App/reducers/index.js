import home from './homeTest';
import {combineReducers} from 'redux';

const rootReducers= combineReducers({
 home
});
export default rootReducers;