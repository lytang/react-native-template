/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, View} from 'react-native';
// import Reducer from './App/reducers'
import {Home} from '@container';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import {configureStores} from '@common';
// const store = createStore(Reducer)
let store = configureStores()
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <Home/>
      </Provider>
    );
  }
}

